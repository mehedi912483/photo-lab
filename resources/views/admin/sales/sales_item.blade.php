@extends('layouts.admin')
@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Sales</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Add Sales
                                    <a href="{{ route('sales.index') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="la flaticon-back"></i> Back</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <tr>
                                        <td>Date</td>
                                        <td>{{ $salesInfo->date }}</td>
                                        <td>Customer</td>
                                        <td>{{ $salesInfo->customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Item Type</td>
                                        <td>
                                            @if($salesInfo->item_type == 1)
                                                Machinaries
                                            @else
                                                Spare Parts
                                            @endif
                                        </td>
                                        <td>Discount</td>
                                        <td>{{ $salesInfo->discount }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                @if(!empty($salesDetails))
                                    {!! Form::model($salesDetails, ['route' => ['sale-item-update', $salesDetails->id]]) !!}
                                    @method('put')
                                @else
                                {!! Form::open(['route' => ['sales.item.store', $salesInfo->id]]) !!}
                                @endif
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category') !!}
                                    {!! Form::select('category_id', $categories, null, ['id' => 'category_id', 'placeholder'=>'Select Category','class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('item_id', 'Item') !!}
                                    {!! Form::select('item_id', $items, null, ['id' => 'item_id', 'placeholder'=>'Select Item','class' => 'form-control']) !!}
                                    @error('item_id')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    {!! Form::label('quantity', 'Quantity') !!}
                                    {!! Form::number('quantity', null, ['id' => 'quantity', 'placeholder'=>'Quantity','class' => 'form-control']) !!}
                                    @error('quantity')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    {!! Form::label('unit_price', 'Unit Price') !!}
                                    {!! Form::number('unit_price', null, ['id' => 'unit_price', 'placeholder'=>'Unit Price','class' => 'form-control']) !!}
                                    @error('unit_price')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> add</button>
                                        </div>
                                    </div>
                                </div>

                                    {!! Form::close() !!}
                            </div>
                        </div>
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <table  class="table table-bordered">
                                        <thead>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                        </thead>
                                        <tbody class="item-table">
                                        @foreach($salesList as $index => $v)
                                            <tr>
                                                <td>{{ $v->item->item_name }}</td>
                                                <td>{{ $v->quantity }}</td>
                                                <td>{{ $v->unit_price }}</td>
                                                <td>{{ $v->total }}</td>
                                                <td>
                                                    <a href="{{ route('sales.details.edit', [$salesInfo->id, $v->id]) }}" class="item-edit btn btn-success btn-xs ">
                                                        <i class="fas fa-pen-square" title="Edit"></i>
                                                    </a>
                                                   &nbsp;||&nbsp;

                                                    <form class="d-inline" action="{{ route('sales.details.destroy', [$salesInfo->id, $v->id]) }}" method="post">
                                                        @csrf
                                                        @method('get')
                                                        <button type="submit" class="btn btn-danger btn-xs delete-button">
                                                            <i class="fas fa-trash-alt" title="Delete"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            var _this = $(this);
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                if (result.isConfirmed) _this.closest('form').submit();
            });
        });

        $('#category_id').on('change', function (){
            var category_id = $(this).val();
            $.ajax({
                url:"{{url('admin/items-by-category')}}/"+category_id,
                type:"GET",
                dataType:'json',
                success:function(data){
                    $('#item_id').html('<option value="">Select Item</option>')
                    $.each( data, function( key, value) {
                        $('#item_id').append('<option value="'+value.id+'">'+value.item_name+'</option>')
                    });
                }
            });
        });
    </script>
@endpush
