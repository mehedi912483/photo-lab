<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date', 'Date')  !!} <span class="text-danger">*</span>
            {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Y-m-d']) !!}

            @error('date')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('supplier_id', 'Supplier') !!} <span class="text-danger">*</span>
            {!! Form::select('supplier_id', $suppliers, null, ['id' => 'supplier_id', 'placeholder'=>'Select Supplier','class' => 'form-control']) !!}
            @error('supplier_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('purchase_id', 'Purchase Id') !!} <span class="text-danger">*</span>
            {!! Form::text('purchase_id', null, ['id' => 'purchase_id', 'class' => 'form-control']) !!}
            <span class="total_due text-success">Total Due : {{ @$totalDue }}</span>
            @error('purchase_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('amount', 'Amount') !!} <span class="text-danger">*</span>
            {!! Form::number('amount', null, ['id' => 'amount', 'class' => 'form-control']) !!}
            @error('amount')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'style' => 'height:115px']) !!}
        </div>
    </div>
</div>
@push('js')
<script>
    $('#purchase_id').on('keyup', function (){
        var purchase_id = $(this).val();
        $.ajax({
            url:"{{url('admin/invoice_due')}}/"+purchase_id,
            type:"GET",
            dataType:'json',
            success:function(data){
                $('.total_due').text("Total Due : "+data.totaldue);
            }
        });
    });
</script>
@endpush


