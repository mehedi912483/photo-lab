@extends('layouts.admin')

@section('content')
<div class="main-panel">
      <div class="content">
        <div class="page-inner">
          <div class="page-header">
            <ul class="breadcrumbs">
              <li class="nav-home">
                <a href="#">
                  <i class="flaticon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Admin</a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Bill Payment</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">
                    Bill Payment
                    <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#exampleModal">
                      <i class="fas fa-plus-circle"></i> add
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Supplier Name</th>
                        <th>Amount</th>
                        <th>Remarks</th>
                        <th>Status</th>
                        <th width="140">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>Md Kamal</td>
                        <td>100.00</td>
                        <td>Comments</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Md Kamal</td>
                        <td>100.00</td>
                        <td>Comments</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Md Kamal</td>
                        <td>100.00</td>
                        <td>Comments</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>Md Kamal</td>
                        <td>100.00</td>
                        <td>Comments</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Bill Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
            	<div class="form-group">
	                <label for="comment">Supplier Name</label>
	                <select class="form-control" id="exampleFormControlSelect1">
                    <option value="1">Select Supplier</option>
                    <option value="1">supplier-1</option>
                    <option value="1">supplier-2</option>
                    <option value="1">supplier-3</option>
                    <option value="1">supplier-4</option>
                    <option value="1">supplier-5</option>
                  </select>
	            </div>
	            <div class="form-group">
	                <label for="comment">Purchase Id</label>
	                <input type="number" class="form-control" id="cat_name" placeholder="sales id">
	            </div>
              <div class="form-group">
                  <label for="comment">Amount</label>
                  <input type="number" class="form-control" id="cat_name" placeholder="0.00">
              </div>
              <div class="form-group">
                <label for="comment">Remarks</label>
                <textarea class="form-control" id="comment" rows="2"></textarea>
              </div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Status</label>
                <select class="form-control" id="exampleFormControlSelect1">
                  <option value="1">Active</option>
                  <option value="2">Disable</option>
                </select>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection


@push('js')
<!-- Chart JS -->
<script src="{{ asset('assets/js/plugin/chart.js/chart.min.js') }}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Chart Circle -->
<script src="{{ asset('assets/js/plugin/chart-circle/circles.min.js') }}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('assets/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

<!-- Google Maps Plugin -->
<script src="{{ asset('assets/js/plugin/gmaps/gmaps.js') }}"></script>
@endpush