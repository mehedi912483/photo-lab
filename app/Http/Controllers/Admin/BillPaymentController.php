<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BillPayment;
use App\Models\Purchase;
use App\Models\PurchaseDetails;
use App\Models\Supplier;
use Auth;
use Illuminate\Http\Request;
use DB;
use Exception;

class BillPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['billPayments'] = BillPayment::with('supplier')->paginate(20);
        return view('admin.bill_payment.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['suppliers'] = Supplier::pluck('name', 'id');
        return view('admin/bill_payment/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'supplier_id' => ['required', 'integer', 'max:10'],
                'purchase_id' => ['required', 'integer', 'max:50'],
                'amount' => ['required'],
            ]
        );
        $billpayment = new BillPayment();
        $billpayment->fill($request->all());
        $billpayment->created_by = Auth::user()->id;
        $billpayment->save();

        toast('Data save successfully!', 'success');

        return back();
    }

     /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BillPayment $billPayment)
    {
        $data['suppliers'] = Supplier::pluck('name', 'id');
        $invoiceTotal = Purchase::where('id', $billPayment->purchase_id)->sum('grant_total');
        $PaymentTotal = BillPayment::where('purchase_id', $billPayment->purchase_id)->sum('amount');

        $totalDue = ($invoiceTotal - $PaymentTotal);
        $data['totalDue'] = $totalDue;
        $data['billPayment'] = $billPayment;

        return view('admin/bill_payment/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, BillPayment $billPayment)
    {
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'supplier_id' => ['required', 'integer', 'max:10'],
                'purchase_id' => ['required', 'integer', 'max:50'],
                'amount' => ['required'],
            ]
        );
        $billPayment->fill($request->all());
        $billPayment->created_by = Auth::user()->id;
        $billPayment->save();

        toast('Data update successfully!', 'success');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillPayment $billPayment)
    {
        $billPayment->delete();
        toast('Bill Payment has been deleted successfully!', 'success');

        return redirect()->route('bill-payment.index');
    }

    public function invoiceDue($purchase_id)
    {
        $invoiceTotal = Purchase::where('id', $purchase_id)->sum('grant_total');
        $PaymentTotal = BillPayment::where('purchase_id', $purchase_id)->sum('amount');

        $totalDue = ($invoiceTotal - $PaymentTotal);
        $due = array('totaldue' => $totalDue);

        return response()->json($due);
    }
}
