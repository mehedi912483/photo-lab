<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>.::photolab::.</title>

    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'/>
    <link rel="icon" href="{{ asset('assets/img/icon.ico') }}" type="image/x-icon"/>

    <!-- Fonts and icons -->
    <script src="{{ asset('assets/js/plugin/webfont/webfont.min.js') }}"></script>

    <script>
        WebFont.load({
            google: {"families": ["Open+Sans:300,400,600,700"]},
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"],
                urls: ['{{ asset("assets/css/fonts.css") }}']
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css ') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/azzara.min.css ') }}">



@stack('css')

<!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css ') }}">
</head>
<body>
<div class="wrapper">
    <!--
      Tip 1: You can change the background color of the main header using: data-background-color="blue | purple | light-blue | green | orange | red"
    -->
    <div class="main-header" data-background-color="purple">
        <!-- Logo Header -->
        <div class="logo-header">

            <a href="{{ url('admin/dashboard') }}" class="logo" style="color: #FFF;">
            <!-- <img src="{{ asset('assets/img/logoazzara.svg ') }}" alt="navbar brand" class="navbar-brand"> -->
                Photo Lab
            </a>
            <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
                    data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
            <i class="fa fa-bars"></i>
          </span>
            </button>
            <button class="topbar-toggler more"><i class="fa fa-ellipsis-v"></i></button>
            <div class="navbar-minimize">
                <button class="btn btn-minimize btn-rounded">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
        </div>
        <!-- End Logo Header -->

        <!-- Navbar Header -->
        <nav class="navbar navbar-header navbar-expand-lg">

            <div class="container-fluid">
                <div class="collapse" id="search-nav">
                    <form class="navbar-left navbar-form nav-search mr-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="submit" class="btn btn-search pr-1">
                                    <i class="fa fa-search search-icon"></i>
                                </button>
                            </div>
                            <input type="text" placeholder="Search ..." class="form-control">
                        </div>
                    </form>
                </div>
                <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                    <li class="nav-item toggle-nav-search hidden-caret">
                        <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button"
                           aria-expanded="false" aria-controls="search-nav">
                            <i class="fa fa-search"></i>
                        </a>
                    </li>
                    <li class="nav-item dropdown hidden-caret">
                        <a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-envelope"></i>
                        </a>
                        <ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
                            <li>
                                <div class="dropdown-title d-flex justify-content-between align-items-center">
                                    Messages
                                    <a href="#" class="small">Mark all as read</a>
                                </div>
                            </li>
                            <li>
                                <div class="message-notif-scroll scrollbar-outer">
                                    <div class="notif-center">
                                        <a href="#">
                                            <div class="notif-img">
                                                <img src="{{ asset('assets/img/jm_denis.jpg ') }}" alt="Img Profile">
                                            </div>
                                            <div class="notif-content">
                                                <span class="subject">Jimmy Denis</span>
                                                <span class="block">
                            How are you ?
                          </span>
                                                <span class="time">5 minutes ago</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="notif-img">
                                                <img src="{{ asset('assets/img/chadengle.jpg ') }}" alt="Img Profile">
                                            </div>
                                            <div class="notif-content">
                                                <span class="subject">Chad</span>
                                                <span class="block">
                            Ok, Thanks !
                          </span>
                                                <span class="time">12 minutes ago</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="notif-img">
                                                <img src="{{ asset('assets/img/mlane.jpg ') }}" alt="Img Profile">
                                            </div>
                                            <div class="notif-content">
                                                <span class="subject">Jhon Doe</span>
                                                <span class="block">
                            Ready for the meeting today...
                          </span>
                                                <span class="time">12 minutes ago</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="notif-img">
                                                <img src="{{ asset('assets/img/talha.jpg ') }}" alt="Img Profile">
                                            </div>
                                            <div class="notif-content">
                                                <span class="subject">Talha</span>
                                                <span class="block">
                            Hi, Apa Kabar ?
                          </span>
                                                <span class="time">17 minutes ago</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="see-all" href="javascript:void(0);">See all messages<i
                                        class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown hidden-caret">
                        <a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="notification">4</span>
                        </a>
                        <ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
                            <li>
                                <div class="dropdown-title">You have 4 new notification</div>
                            </li>
                            <li>
                                <div class="notif-scroll scrollbar-outer">
                                    <div class="notif-center">
                                        <a href="#">
                                            <div class="notif-icon notif-primary"><i class="fa fa-user-plus"></i></div>
                                            <div class="notif-content">
                          <span class="block">
                            New user registered
                          </span>
                                                <span class="time">5 minutes ago</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="notif-icon notif-success"><i class="fa fa-comment"></i></div>
                                            <div class="notif-content">
                          <span class="block">
                            Rahmad commented on Admin
                          </span>
                                                <span class="time">12 minutes ago</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="notif-img">
                                                <img src="{{ asset('assets/img/profile2.jpg ') }}" alt="Img Profile">
                                            </div>
                                            <div class="notif-content">
                          <span class="block">
                            Reza send messages to you
                          </span>
                                                <span class="time">12 minutes ago</span>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="notif-icon notif-danger"><i class="fa fa-heart"></i></div>
                                            <div class="notif-content">
                          <span class="block">
                            Farrah liked Admin
                          </span>
                                                <span class="time">17 minutes ago</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="see-all" href="javascript:void(0);">See all notifications<i
                                        class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown hidden-caret">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                            <div class="avatar-sm">
                                <img src="{{ asset('assets/img/profile.jpg ') }}" alt="..."
                                     class="avatar-img rounded-circle">
                            </div>
                        </a>
                        <ul class="dropdown-menu dropdown-user animated fadeIn">
                            <li>
                                <div class="user-box">
                                    <div class="avatar-lg"><img src="{{ asset('assets/img/profile.jpg ') }}"
                                                                alt="image profile" class="avatar-img rounded"></div>
                                    <div class="u-text">
                                        <h4>Hizrian</h4>
                                        <p class="text-muted">hello@example.com</p><a href="profile.html"
                                                                                      class="btn btn-rounded btn-danger btn-sm">View
                                            Profile</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">My Profile</a>
                                <a class="dropdown-item" href="#">My Balance</a>
                                <a class="dropdown-item" href="#">Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Logout</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
        <!-- End Navbar -->
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <div class="sidebar-background"></div>
        <div class="sidebar-wrapper scrollbar-inner">
            <div class="sidebar-content">
                <div class="user">
                    <div class="avatar-sm float-left mr-2">
                        <img src="{{ asset('assets/img/profile.jpg ') }}" alt="..." class="avatar-img rounded-circle">
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                <span>
                  Hizrian
                  <span class="user-level">Administrator</span>
                  <span class="caret"></span>
                </span>
                        </a>
                        <div class="clearfix"></div>

                        <div class="collapse in" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="#profile">
                                        <span class="link-collapse">My Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#edit">
                                        <span class="link-collapse">Edit Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#settings">
                                        <span class="link-collapse">Settings</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">
                    <li class="nav-item active">
                        <a href="{{ url('admin/dashboard') }}">
                            <i class="fas fa-home"></i>
                            <p>Dashboard</p>
                            <span class="badge badge-count">5</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#custompages">
                            <i class="flaticon-settings"></i>
                            <p>Master </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="custompages">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/category') }}">
                                        <span class="sub-item">Catagory</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/item') }}">
                                        <span class="sub-item">Item</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/supplier') }}">
                                        <span class="sub-item">Supplier</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/customer') }}">
                                        <span class="sub-item">Customer</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/designation') }}">
                                        <span class="sub-item">Designation</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#stockdata">
                            <i class="fas fa-desktop"></i>
                            <p>Stock </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="stockdata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/opening-stock') }}">
                                        <span class="sub-item">Opening Stock</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/stock-reject') }}">
                                        <span class="sub-item">Stock Reject</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#purchasdata">
                            <i class="fas fa-layer-group"></i>
                            <p>Purchase </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="purchasdata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/purchase') }}">
                                        <span class="sub-item">Purchase List</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('purchase.create') }}">
                                        <span class="sub-item">Purchase</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/bill-payment') }}">
                                        <span class="sub-item">Bill Payment</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#saledata">
                            <i class="fas fa-anchor"></i>
                            <p>Sales </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="saledata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/sales') }}">
                                        <span class="sub-item">Sales List</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/sales') }}">
                                        <span class="sub-item">Sale</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/sales-expense') }}">
                                        <span class="sub-item">Sale Expense</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/money-receipt') }}">
                                        <span class="sub-item">Money Receipt</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#gatepassdata">
                            <i class="fas fa-adjust"></i>
                            <p>Gate Pass </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="gatepassdata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/gate-pass') }}">
                                        <span class="sub-item">Gate Pass</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#servicingdata">
                            <i class="fas fa-atlas"></i>
                            <p>Servicing </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="servicingdata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/servicing-request') }}">
                                        <span class="sub-item">Servicing Request</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/engineer-assign') }}">
                                        <span class="sub-item">Engineer Assign</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/get-challan') }}">
                                        <span class="sub-item">Get Challan</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#hrdata">
                            <i class="fas fa-atom"></i>
                            <p>HR </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="hrdata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/stuff') }}">
                                        <span class="sub-item">Stuff</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/attendance') }}">
                                        <span class="sub-item">Attendance</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/leave') }}">
                                        <span class="sub-item">Leave</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/salary_generate') }}">
                                        <span class="sub-item">Salary Generate</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/salary') }}">
                                        <span class="sub-item">Salary List</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="collapse" href="#accountdata">
                            <i class="fas fa-paint-roller"></i>
                            <p>ACCOUNT </p>
                            <span class="caret"></span>
                        </a>
                        <div class="collapse" id="accountdata">
                            <ul class="nav nav-collapse">
                                <li>
                                    <a href="{{ url('admin/account-head') }}">
                                        <span class="sub-item">Account Head</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/expense') }}">
                                        <span class="sub-item">Expense</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/stuff-payment') }}">
                                        <span class="sub-item">Stuff Payment</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/day-clossing') }}">
                                        <span class="sub-item">Day Closing</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Sidebar -->

    @yield('content')


</div>
</div>

@include('sweetalert::alert')

<!--   Core JS Files   -->
<script src="{{ asset('assets/js/core/jquery.3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<!-- jQuery UI -->
<script src="{{ asset('assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

<!-- jQuery Scrollbar -->
<script src="{{ asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

<!-- Moment JS -->
<script src="{{ asset('assets/js/plugin/moment/moment.min.js') }}"></script>


<!-- Datatables -->
<script src="{{ asset('assets/js/plugin/datatables/datatables.min.js') }}"></script>

<!-- Bootstrap Notify -->
<script src="{{ asset('assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<!-- Bootstrap Toggle -->
<script src="{{ asset('assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>

<script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>


<!-- Azzara JS -->
<script src="{{ asset('assets/js/ready.min.js') }}"></script>

<!-- Azzara DEMO methods, don't include it in your project! -->
<!-- <script src="{{ asset('assets/js/setting-demo.js') }}"></script>
<script src="{{ asset('assets/js/demo.js') }}"></script> -->


@stack('js')
<script>
    $('.date').datepicker({
        format: 'mm-dd-yyyy'
    });
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
</script>
</body>

</html>
