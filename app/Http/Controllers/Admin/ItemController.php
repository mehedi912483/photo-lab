<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Category;
use Auth;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['items'] = Item::with('category')->paginate(20);
        return view('admin.item.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('category_name', 'id');

        return view('admin/item/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'category_id' => ['required', 'string', 'max:100'],
                'item_name' => ['required', 'string', 'max:200'],
            ]
        );
        $item = new Item();
        $item->category_id = $request->category_id;
        $item->item_name   = $request->item_name;
        $item->item_type   = $request->item_type;
        $item->description = $request->description;
        $item->created_by  = Auth::user()->id;
        $item->save();

        toast('Item has been save successfully!','success');

        return redirect()->route('item.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['item'] = $item;
        return view('admin/item/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $request->validate(
            [
                'category_id' => ['required', 'string', 'max:100'],
                'item_name' => ['required', 'string', 'max:200'],
            ]
        );
        $item->category_id = $request->category_id;
        $item->item_name   = $request->item_name;
        $item->item_type   = $request->item_type;
        $item->description = $request->description;
        $item->created_by  = Auth::user()->id;
        $item->save();

        toast('Item has been updated successfully!','success');

        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();

        toast('Item has been deleted successfully!','success');

        return redirect()->route('item.index');
    }
}
