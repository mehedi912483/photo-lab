@extends('layouts.admin')

@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Supplier</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Supplier Name
                                    <a href="{{ route('supplier.create') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="fas fa-plus-circle"></i> add
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Company</th>
                                        <th>Contact Person</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th width="140">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($supplires as $index => $v)
                                        <tr>
                                            <th scope="row">{{ $supplires->firstItem() + $index }}</th>
                                            <td>{{ $v->name }}</td>
                                            <td>{{ $v->phone }}</td>
                                            <td>{{ $v->email }}</td>
                                            <td>{{ $v->company_name }}</td>
                                            <td>{{ $v->contact_person }}</td>
                                            <td>{{ $v->address }}</td>
                                            <td>
                                                @if($v->status == 1)
                                                    Active
                                                @else
                                                    Disabled
                                                @endif
                                            </td>
                                            <td>

                                                <form action="{{ route('supplier.destroy', $v->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <a href="{{ route('supplier.edit', $v->id) }}" class="btn btn-secondary btn-xs">
                                                        <i class="fas fa-pen-square"></i> Edit
                                                    </a>
                                                    <button class="btn btn-danger btn-xs delete-button">
                                                        <i class="fas fa-trash-alt"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $supplires->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                console.log(result)
                if (result.isConfirmed) $('.delete-button').parents('form').submit();
            });
        });
    </script>
@endpush
