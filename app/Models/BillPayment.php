<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillPayment extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['date', 'supplier_id', 'purchase_id', 'amount', 'description'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->select('id', 'name');

    }
}
