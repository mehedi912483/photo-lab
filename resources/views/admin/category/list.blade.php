@extends('layouts.admin')

@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Category</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Category Name
                                    <a href="{{ route('category.create') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="fas fa-plus-circle"></i> add
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category Name</th>
                                        <th>Description</th>
                                        <th class="text-center">Status</th>
                                        <th width="140">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $index => $v)
                                        <tr>
                                            <th scope="row">{{ $categories->firstItem() + $index }}</th>
                                            <td>{{ $v->category_name }}</td>
                                            <td>{{ $v->description }}</td>
                                            <td class="text-center">
                                                @if($v->status == 1)
                                                    <button type="button" class="btn btn-icon btn-round btn-success">
                                                        <i class="la flaticon-interface-1"></i>
                                                    </button>
                                                @else
                                                    <button type="button" class="btn btn-icon btn-round btn-danger">
                                                        <i class="la flaticon-cross-1"></i>
                                                    </button>
                                                @endif
                                            </td>
                                            <td>

                                                <form action="{{ route('category.destroy', $v->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <a href="{{ route('category.edit', $v->id) }}" class="btn btn-secondary btn-xs">
                                                        <i class="fas fa-pen-square"></i> Edit
                                                    </a>
                                                    <button class="btn btn-danger btn-xs delete-button">
                                                        <i class="fas fa-trash-alt"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $categories->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                console.log(result)
                if (result.isConfirmed) $('.delete-button').parents('form').submit();
            });
        });
    </script>
@endpush
