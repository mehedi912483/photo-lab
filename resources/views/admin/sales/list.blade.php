@extends('layouts.admin')

@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Sales</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Sales
                                    <a href="{{ route('sales.create') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="fas fa-plus-circle"></i> add
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Customer Name</th>
                                        <th>Item Type</th>
                                        <th>Discount</th>
                                        <th>Grand Total</th>
                                        <th>Remarks</th>
                                        <th width="140">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($salesList as $index => $v)
                                        <tr>
                                            <th scope="row">{{ $salesList->firstItem() + $index }}</th>
                                            <td>{{ $v->date }}</td>
                                            <td>{{ $v->customer->name }}</td>
                                            <td>
                                                @if($v->item_type == 1)
                                                    Machinaries
                                                @else
                                                    Spare Parts
                                                @endif
                                            </td>
                                            <td>{{ $v->discount }}</td>
                                            <td>{{ $v->grand_total }}</td>
                                            <td>{{ $v->description }}</td>
                                            <td>
                                                <a href="{{ route('sales.edit', $v->id) }}" class="btn btn-secondary btn-xs">
                                                    <i class="fas fa-pen-square" title="Edit"></i>
                                                </a>

                                                <form class="d-inline" action="{{ route('sales.destroy', $v->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-danger btn-xs delete-button">
                                                        <i class="fas fa-trash-alt" title="Delete"></i>
                                                    </button>
                                                </form>
                                                <a href="{{ route('sales.show', $v->id) }}" class="btn btn-primary btn-xs">
                                                    <i class="la flaticon-medical" title="View"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $salesList->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
   <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            var _this = $(this);
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                if (result.isConfirmed) _this.closest('form').submit();
            });
            $('.date').datepicker({
                format: 'mm-dd-yyyy'
            });
        });
    </script>
@endpush
