<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('category_name', 'Name')  !!} <span class="text-danger">*</span>
            {!! Form::text('category_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Category Name', 'id' => 'category_name']) !!}

            @error('category_name')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Status') !!} <span class="text-danger">*</span>
            {!! Form::select('status', ['1' => 'Active', '2' => 'Disable'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
