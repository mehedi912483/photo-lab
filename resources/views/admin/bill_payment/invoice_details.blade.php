@extends('layouts.admin')

@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Purchases</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Purchase
                                    <a href="{{ route('purchase.create') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="fas fa-plus-circle"></i> add
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item Name</th>
                                        <th>Item Type</th>
                                        <th>Quantity</th>
                                        <th>Rate</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchaseDetails as $index => $v)
                                        <tr>
                                            <th scope="row">{{ $index +1 }}</th>
                                            <td>{{ $v->item->item_name }}</td>
                                            <td>
                                                @if($v->item_type == 1)
                                                    Machinaries
                                                @else
                                                    Spare Parts
                                                @endif
                                            </td>
                                            <td>{{ $v->quantity }}</td>
                                            <td>{{ $v->unit_price }}</td>
                                            <td>{{ $v->total }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
   <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            var _this = $(this);
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                if (result.isConfirmed) _this.closest('form').submit();
            });
            $('.date').datepicker({
                format: 'mm-dd-yyyy'
            });
        });
    </script>
@endpush
