<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date', 'Date')  !!} <span class="text-danger">*</span>
            {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Y-m-d']) !!}

            @error('date')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('customer_id', 'Customer') !!} <span class="text-danger">*</span>
            {!! Form::select('customer_id', $customers, null, ['id' => 'customer_id', 'placeholder'=>'Select Customer','class' => 'form-control']) !!}
            @error('customer_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('sale_id', 'Sale Id') !!} <span class="text-danger">*</span>
            {!! Form::text('sale_id', null, ['id' => 'sale_id', 'class' => 'form-control']) !!}
            <span class="total_due text-success">Total Due : {{ @$totalDue }}</span>
            @error('sale_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('amount', 'Amount') !!} <span class="text-danger">*</span>
            {!! Form::number('amount', null, ['id' => 'amount', 'class' => 'form-control']) !!}
            @error('amount')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'style' => 'height:115px']) !!}
        </div>
    </div>
</div>
@push('js')
<script>
    $('#sale_id').on('keyup', function (){
        var sale_id = $(this).val();
        $.ajax({
            url:"{{url('admin/sale_invoice_due')}}/"+sale_id,
            type:"GET",
            dataType:'json',
            success:function(data){
                $('.total_due').text("Total Due : "+data.totaldue);
            }
        });
    });
</script>
@endpush


