<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['date', 'customer_id', 'item_type', 'discount', 'grand_total', 'description'];

    public function customer()
    {
        return $this->belongsTo(Customer::class)->select('id', 'name');

    }

    public function customerInfo()
    {
        return $this->belongsTo(Customer::class)->select('name', 'phone', 'email', 'company_name', 'contact_person', 'address');

    }
    public function categories()
    {
        return $this->belongsTo(Category::class)->select('id', 'category_name');

    }

    public function items()
    {
        return $this->belongsTo(Item::class)->select('id', 'item_name');

    }

    public function salesDetails()
    {
        return $this->hasMany(SalesDetails::class)->select('item_id', 'quantity', 'unit_price');
    }

}


