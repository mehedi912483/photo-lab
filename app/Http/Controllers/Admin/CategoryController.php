<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Auth;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data['categories'] = Category::paginate(2);

        return view('admin/category/list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin/category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'category_name' => 'required|unique:categories,category_name',
            ]
        );

        $category = new Category();

        $category->category_name = $request->category_name;
        $category->description   = $request->description;
        $category->status        = $request->status;
        $category->created_by    = Auth::user()->id;
        $category->save();

        toast('Category has been saved successfully!','success');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Category $category)
    {
        $data['category'] = $category;

        return view('admin/category/edit_form', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Category $category)
    {
        $data['category'] = $category;

        return view('admin/category/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Category $category)
    {
        $request->validate(
            [
                'category_name' => ['required', 'string', 'max:100', 'unique:categories,category_name,' . $category->id . ',id'],
            ]
        );

        $category->category_name = $request->category_name;
        $category->description   = $request->description;
        $category->status        = $request->status;
        $category->created_by    = Auth::user()->id;
        $category->save();

        toast('Category has been updated successfully!','success');

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();

        toast('Category has been deleted successfully!','success');

        return redirect()->route('category.index');
    }
}
