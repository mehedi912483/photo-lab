<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('date', 'Date')  !!} <span class="text-danger">*</span>
            {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Y-m-d']) !!}

            @error('category_name')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('customer_id', 'Customer') !!} <span class="text-danger">*</span>
            {!! Form::select('customer_id', $customers, null, ['id' => 'customer_id', 'placeholder'=>'Select Customer','class' => 'form-control']) !!}
            @error('customer_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('item_type', 'Item Type') !!} <span class="text-danger">*</span>
            {!! Form::select('item_type', ['1' => 'Machinaries', '2' => 'Spare Parts'], null, ['class' => 'form-control']) !!}
            @error('item_type')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('discount', 'Discount') !!}
            {!! Form::number('discount', null, ['id' => 'discount', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'style' => 'height:115px']) !!}
        </div>
    </div>
</div>
@push('js')

@endpush


