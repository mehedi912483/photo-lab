<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('category_id', 'Category') !!} <span class="text-danger">*</span>
            {!! Form::select('category_id', $categories, null, ['id' => 'category_id', 'placeholder'=>'Select Category','class' => 'form-control']) !!}
            @error('category_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('item_id', 'Item') !!} <span class="text-danger">*</span>
            {!! Form::select('item_id', $items, null, ['id' => 'item_id', 'placeholder'=>'Select Item','class' => 'form-control']) !!}
            @error('category_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('quantity', 'Quantity')  !!} <span class="text-danger">*</span>
            {!! Form::text('quantity', null, ['class' => 'form-control', 'placeholder' => 'Enter Quantity', 'id' => 'quantity']) !!}

            @error('quantity')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Status') !!}
            {!! Form::select('status', ['1' => 'Active', '2' => 'Disabled'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
</div>
@push('js')
    <script>
        $('#category_id').on('change', function (){
            var category_id = $(this).val();
            $.ajax({
                url:"{{url('admin/items-by-category')}}/"+category_id,
                type:"GET",
                dataType:'json',
                success:function(data){
                    $('#item_id').html('<option value="">Select Item</option>')
                    $.each( data, function( key, value) {
                        $('#item_id').append('<option value="'+value.id+'">'+value.item_name+'</option>')
                    });
                }
            });
         });
    </script>
@endpush


