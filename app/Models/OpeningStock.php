<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpeningStock extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['category_id', 'item_id', 'date', 'quantity', 'description', 'status'];
    public function category()
    {
        return $this->belongsTo(Category::class)->select('id', 'category_name');

    }

    public function item()
    {
        return $this->belongsTo(Item::class)->select('id', 'item_name');

    }


}
