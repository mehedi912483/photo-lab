<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Category;
use App\Models\StockReject;
use App\Models\Stock;
use Auth;
use Illuminate\Http\Request;
use DB;
use Exception;

class StockRejectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['stockRejects'] = StockReject::with('category')->with('item')->paginate(20);
        return view('admin.stock_reject.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');

        return view('admin/stock_reject/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'category_id' => ['required', 'string', 'max:300'],
                'item_id' => ['required', 'string', 'max:300'],
                'quantity' => ['required', 'integer'],
            ]
        );

        try {
            DB::beginTransaction();

            $stockReject = new StockReject();
            $stockReject->fill($request->all());
            $stockReject->date = date('Y-m-d');
            $stockReject->created_by = Auth::user()->id;
            $stockReject->save();

            $stock = Stock::deductStockReject($request->item_id, $request->quantity);

            DB::commit();

            toast('Stock reject has been save successfully!', 'success');

            return redirect()->route('stock-reject.index');

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(StockReject $stockReject)
    {
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['stockReject']  = $stockReject;
        return view('admin/stock_reject/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockReject $stockReject)
    {
        $request->validate(
            [
                'category_id' => ['required', 'string', 'max:300'],
                'item_id' => ['required', 'string', 'max:300'],
                'quantity' => ['required', 'integer'],
            ]
        );
        try {
            DB::beginTransaction();

            $oldQuantity = $stockReject->quantity;
            $stockReject->fill($request->all());
            $stockReject->date = date('Y-m-d');
            $stockReject->created_by = Auth::user()->id;
            $stockReject->save();

            $stock = Stock::updateStockReject($request->item_id, $request->quantity, $oldQuantity);

            DB::commit();

            toast('Stock reject has been update successfully!', 'success');

            return redirect()->route('stock-reject.index');

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockReject $stockReject)
    {

        $stock = Stock::addStock($stockReject->item_id, $stockReject->quantity);
        $stockReject->delete();

        toast('Stock reject has been deleted successfully!', 'success');

        return redirect()->route('stock-reject.index');
    }

    public function itemsByCategory($catId)
    {
        $items = Item::where('category_id', $catId)->get();

        return response()->json($items);
    }
}
