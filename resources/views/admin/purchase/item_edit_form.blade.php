<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            {!! Form::label('category_id', 'Category') !!}
            {!! Form::select('category_id', $categories, null, ['id' => 'category_id','name' => 'category_id', 'placeholder'=>'Select Category','class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('item_id', 'Item') !!}
            {!! Form::select('item_id', $items, null, ['id' => 'item_id', 'placeholder'=>'Select Item','class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('quantity', 'Quantity') !!}
            {!! Form::number('quantity', null, ['id' => 'quantity', 'placeholder'=>'Quantity','class' => 'form-control']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('unit_price', 'Quantity') !!}
            {!! Form::number('unit_price', null, ['id' => 'unit_price', 'placeholder'=>'Unit Price','class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-7">
        <table  class="table table-bordered">
            <thead>
            <th>Category</th>
            <th>Item</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total</th>
            <th>Action</th>
            </thead>
            <tbody class="item-table">

            </tbody>
        </table>
    </div>
    </div>
</div>
@push('js')
    <script>
        $('#category_id').on('change', function (){
            var category_id = $(this).val();
            $.ajax({
                url:"{{url('admin/items-by-category')}}/"+category_id,
                type:"GET",
                dataType:'json',
                success:function(data){
                    $('#item_id').html('<option value="">Select Item</option>')
                    $.each( data, function( key, value) {
                        $('#item_id').append('<option value="'+value.id+'">'+value.item_name+'</option>')
                    });
                }
            });
         });

        $('#item_id').on('change', function (){
            var item_id = $(this).val();
            $.ajax({
                url:"{{url('admin/item-info')}}/"+item_id,
                type:"GET",
                dataType:'json',
                success:function(data){
                   console.log(data);
                }
            });
        });
        $('.date').datepicker({
            format: 'mm-dd-yyyy'
        });
    </script>
@endpush


