<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['category_id', 'item_name', 'item_type', 'description', 'status'];

    public function category()
    {
        return $this->belongsTo(Category::class)->select('id', 'category_name');

    }
}
