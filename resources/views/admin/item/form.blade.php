<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('category_id', 'Category') !!} <span class="text-danger">*</span>
            {!! Form::select('category_id', $categories, null, ['placeholder'=>'Select Category','class' => 'form-control']) !!}
            @error('category_id')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('item_name', 'Item Name')  !!} <span class="text-danger">*</span>
            {!! Form::text('item_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Item Name', 'id' => 'category_name']) !!}

            @error('item_name')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('item_type', 'Item Type') !!}
            {!! Form::select('item_type', ['1' => 'Machinaries', '2' => 'Spare Parts'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
    </div>
</div>
