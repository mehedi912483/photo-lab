@extends('layouts.admin')

@section('content')
<div class="main-panel">
      <div class="content">
        <div class="page-inner">
          <div class="page-header">
            <ul class="breadcrumbs">
              <li class="nav-home">
                <a href="#">
                  <i class="flaticon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Admin</a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Purchase</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">
                    Purchase
                  </div>
                </div>
                <div class="card-body">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="comment">Date</label>
                          <input type="date" class="form-control" id="cat_name" placeholder="Enter Item Name">
                        </div>
                        <div class="form-group">
                          <label for="email2">Suppler Name</label>
                          <select class="form-control" id="exampleFormControlSelect1">
                            <option value="1">Supplier-1</option>
                            <option value="1">Supplier-2</option>
                            <option value="1">Supplier-3</option>
                            <option value="1">Supplier-4</option>
                            <option value="1">Supplier-5</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="email2">Item Type</label>
                          <select class="form-control" id="exampleFormControlSelect1">
                            <option value="1">Select Item Typ</option>
                            <option value="1">Item Type-1</option>
                            <option value="1">Item Type-2</option>
                            <option value="1">Item Type-3</option>
                            <option value="1">Item Type-4</option>
                            <option value="1">Item Type-5</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="comment">Remarks</label>
                          <textarea class="form-control" id="comment" rows="6"></textarea>
                        </div>
                      </div>                      
                  </div>
                  <div class="row">
                    <table  class="table table-bordered">
                      <thead>
                        <th>Category</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Total</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <select class="form-control" id="exampleFormControlSelect1">
                              <option value="1">Select Category</option>
                              <option value="1">Category-1</option>
                              <option value="1">Category-2</option>
                              <option value="1">Category-3</option>
                              <option value="1">Category-4</option>
                              <option value="1">Category-5</option>
                            </select>
                          </td>
                          <td>
                            <select class="form-control" id="exampleFormControlSelect1">
                              <option value="1">Select Item</option>
                              <option value="1">Item-1</option>
                              <option value="1">Item-2</option>
                              <option value="1">Item-3</option>
                              <option value="1">Item-4</option>
                              <option value="1">Item-5</option>
                            </select>
                          </td>
                          <td>
                            <input type="number" name="quantity" id="quantity" class="form-control">
                          </td>
                          <td>
                            <input type="number" name="quantity" id="quantity" class="form-control">
                          </td>
                          <td>
                            <input type="number" name="quantity" id="quantity" class="form-control">
                          </td>
                          <td><button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#exampleModal">
                                  <i class="fas fa-plus-circle"></i> add
                                </button></td>
                        </tr>
                        <tr>
                          <td>Category-1</td>
                          <td>Item-1</td>
                          <td class="text-center">10</td>
                          <td class="text-right">100.00</td>
                          <td class="text-right">100.00</td>
                          <td class="text-center">
                            <a href="#" class="">
                                <i class="fas fa-pen-square"></i>
                            </a> ||
                            <a href="#" class="">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>Category-1</td>
                          <td>Item-1</td>
                          <td class="text-center">10</td>
                          <td class="text-right">100.00</td>
                          <td class="text-right">100.00</td>
                          <td class="text-center">
                            <a href="#" class="">
                                <i class="fas fa-pen-square"></i>
                            </a> ||
                            <a href="#" class="">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>Category-1</td>
                          <td>Item-1</td>
                          <td class="text-center">10</td>
                          <td class="text-right">100.00</td>
                          <td class="text-right">100.00</td>
                          <td class="text-center">
                            <a href="#" class="">
                                <i class="fas fa-pen-square"></i>
                            </a> ||
                            <a href="#" class="">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>Category-1</td>
                          <td>Item-1</td>
                          <td class="text-center">10</td>
                          <td class="text-right">100.00</td>
                          <td class="text-right">100.00</td>
                          <td class="text-center">
                            <a href="#" class="">
                                <i class="fas fa-pen-square"></i>
                            </a> ||
                            <a href="#" class="">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="4" class="text-right">Sub Total</td>
                          <td class="text-center"><input type="number" name="subtoTotal" class="form-control" value="400.00"></td>
                          <td class="text-center"></td>
                        </tr>
                        <tr>
                          <td colspan="4" class="text-right">Discount(%)</td>
                          <td class="text-center"><input type="number" name="subtoTotal" class="form-control" value="50.00"></td>
                          <td class="text-center"></td>
                        </tr>
                        <tr>
                          <td colspan="4" class="text-right">Grand Total</td>
                          <td class="text-center"><input type="number" name="subtoTotal" class="form-control" value="350.00"></td>
                          <td class="text-center"></td>
                        </tr>
                        <tr>
                          <td colspan="5">
                            <button type="Submit" class="btn btn-success btn-md"><i class="flaticon-back"> </i>Back</button>
                          
                            <button type="Submit" class="btn btn-secondary btn-md">Cancel</button>
                         
                            <button type="Submit" class="btn btn-primary btn-md">Submit</button>
                          </td>
                          <td class="text-center"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<!-- Modal -->
@endsection


@push('js')
<!-- Chart JS -->
<script src="{{ asset('assets/js/plugin/chart.js/chart.min.js') }}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Chart Circle -->
<script src="{{ asset('assets/js/plugin/chart-circle/circles.min.js') }}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('assets/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

<!-- Google Maps Plugin -->
<script src="{{ asset('assets/js/plugin/gmaps/gmaps.js') }}"></script>
@endpush