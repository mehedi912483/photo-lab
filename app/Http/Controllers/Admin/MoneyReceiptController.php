<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\MoneyReceipt;
use App\Models\Purchase;
use App\Models\PurchaseDetails;
use App\Models\Sale;
use App\Models\Supplier;
use Auth;
use Illuminate\Http\Request;
use DB;
use Exception;

class MoneyReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['moneyReceipts'] = MoneyReceipt::with('customer')->paginate(20);
        return view('admin.money_receipt.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customers'] = Customer::pluck('name', 'id');
        return view('admin/money_receipt/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'customer_id' => ['required', 'integer', 'max:10'],
                'sale_id' => ['required', 'integer', 'max:50'],
                'amount' => ['required'],
            ]
        );
        $moneyreceipt = new MoneyReceipt();
        $moneyreceipt->fill($request->all());
        $moneyreceipt->created_by = Auth::user()->id;
        $moneyreceipt->save();

        toast('Data save successfully!', 'success');

        return back();
    }

     /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MoneyReceipt $moneyReceipt)
    {
        $data['customers'] = Customer::pluck('name', 'id');
        $invoiceTotal = Sale::where('id', $moneyReceipt->sale_id)->sum('grand_total');
        $PaymentTotal = MoneyReceipt::where('sale_id', $moneyReceipt->sale_id)->sum('amount');

        $totalDue = ($invoiceTotal - $PaymentTotal);
        $data['totalDue'] = $totalDue;
        $data['moenyReceipt'] = $moneyReceipt;

        return view('admin/money_receipt/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, MoneyReceipt $moneyReceipt)
    {
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'customer_id' => ['required', 'integer', 'max:10'],
                'sale_id' => ['required', 'integer', 'max:50'],
                'amount' => ['required'],
            ]
        );
        $moneyReceipt->fill($request->all());
        $moneyReceipt->created_by = Auth::user()->id;
        $moneyReceipt->save();

        toast('Data update successfully!', 'success');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MoneyReceipt $moneyReceipt)
    {
        $moneyReceipt->delete();
        toast('Money receipt has been deleted successfully!', 'success');

        return redirect()->route('money-receipt.index');
    }

    public function invoiceDue($sale_id)
    {

        $invoiceTotal = Sale::where('id', $sale_id)->sum('grand_total');
        $PaymentTotal = MoneyReceipt::where('sale_id', $sale_id)->sum('amount');

        $totalDue = ($invoiceTotal - $PaymentTotal);
        $due = array('totaldue' => $totalDue);

        return response()->json($due);
    }
}
