@extends('layouts.admin')

@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Sales</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Sales
                                    <a href="{{ route('sales.create') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="fas fa-plus-circle"></i> add
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row mb-4">
                                    <div class="col-sm-6">
                                        <h2>Bill To:</h2>
                                        Photo Lab Limited<br>
                                        Badda, Dhaka<br>
                                        01912756614
                                    </div>
                                    <div class="col-sm-6">
                                        Date:{{ date('Y-m-d') }}<br>
                                        Buy Person: {{ $customerInfo->name }}<br>
                                        Invoice No # {{ $sales->id }}
                                    </div>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item Name</th>
                                        <th>Item Type</th>
                                        <th class="text-right">Quantity</th>
                                        <th class="text-right">Rate</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($salesDetails as $index => $v)
                                        <tr>
                                            <th scope="row">{{ $index +1 }}</th>
                                            <td>{{ $v->item->item_name }}</td>
                                            <td>
                                                @if($v->item_type == 1)
                                                    Machinaries
                                                @else
                                                    Spare Parts
                                                @endif
                                            </td>
                                            <td class="text-right">{{ $v->quantity }}</td>
                                            <td class="text-right">{{ $v->unit_price }}</td>
                                            <td class="text-right">{{ $v->total }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="5" class="text-right">Sub Total</td>
                                        <td class="text-right">{{ $invoiceTotal }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Discount(- {{ $sales->discount }} %)</td>
                                        <td class="text-right">{{ $invoiceTotal * $sales->discount / 100 }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Grand Total</td>
                                        <td class="text-right">{{ $sales->grand_total }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Received</td>
                                        <td class="text-right">{{ $totalReceive }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Due</td>
                                        <td class="text-right">{{ $sales->grand_total - $totalReceive }}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
   <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            var _this = $(this);
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                if (result.isConfirmed) _this.closest('form').submit();
            });
            $('.date').datepicker({
                format: 'mm-dd-yyyy'
            });
        });
    </script>
@endpush
