<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Category;
use App\Models\OpeningStock;
use App\Models\Stock;
use Auth;
use Illuminate\Http\Request;
use DB;
use Exception;

class OpeningStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['openingStocks'] = OpeningStock::with('category')->with('item')->paginate(20);
        return view('admin.opening_stock.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');

        return view('admin/opening_stock/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'category_id' => ['required', 'string', 'max:300'],
                'item_id' => ['required', 'string', 'max:300'],
                'quantity' => ['required', 'integer'],
            ]
        );

        try {
            DB::beginTransaction();

            $openingStock = new OpeningStock();
            $openingStock->fill($request->all());
            $openingStock->date = date('Y-m-d');
            $openingStock->created_by = Auth::user()->id;
            $openingStock->save();

            $stock = Stock::addStock($request->item_id, $request->quantity);

            DB::commit();

            toast('Opening stock has been save successfully!', 'success');

            return redirect()->route('opening-stock.index');

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(OpeningStock $openingStock)
    {
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['openingStock']  = $openingStock;
        return view('admin/opening_stock/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OpeningStock $openingStock)
    {
        $request->validate(
            [
                'category_id' => ['required', 'string', 'max:300'],
                'item_id' => ['required', 'string', 'max:300'],
                'quantity' => ['required', 'integer'],
            ]
        );
        try {
            DB::beginTransaction();

            $oldQuantity = $openingStock->quantity;
            $openingStock->fill($request->all());
            $openingStock->date = date('Y-m-d');
            $openingStock->created_by = Auth::user()->id;
            $openingStock->save();

            $stock = Stock::updateStock($request->item_id, $request->quantity, $oldQuantity);

            DB::commit();

            toast('Opening stock has been update successfully!', 'success');

            return redirect()->route('opening-stock.index');

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpeningStock $openingStock)
    {
        $stock = Stock::deductStockReject($openingStock->item_id, $openingStock->quantity);
        $openingStock->delete();

        toast('Openin stock has been deleted successfully!', 'success');

        return redirect()->route('opening-stock.index');
    }

    public function itemsByCategory($catId)
    {
        $items = Item::where('category_id', $catId)->get();

        return response()->json($items);
    }
}
