<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['item_id', 'quantity'];

    public static function addStock($item_id, $quantity)
    {
        $stock = self::where('item_id', $item_id)->first();

        if(!empty($stock)) {
            $stock->quantity += $quantity;
        } else {
            $stock =  new self();
            $stock->item_id = $item_id;
            $stock->quantity = $quantity;
        }
        return $stock->save();
    }

    public static function updateStock($item_id, $quantity, $oldQuantity)
    {
        $stock         = self::where('item_id', $item_id)->first();
        $stock->quantity += $quantity - $oldQuantity;
        return $stock->save();
    }

    public static function deductStockReject($item_id, $quantity)
    {
        $stock = self::where('item_id', $item_id)->first();

        if(!empty($stock)) {
            $stock->quantity -= $quantity;
        } else {
            $stock =  new self();
            $stock->item_id = $item_id;
            $stock->quantity = $quantity;
        }
        return $stock->save();
    }

    public static function updateStockReject($item_id, $quantity, $oldQuantity)
    {
        $stock         = self::where('item_id', $item_id)->first();
        $stock->quantity -= $quantity - $oldQuantity;
        return $stock->save();
    }

}
