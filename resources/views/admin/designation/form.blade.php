<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('designation_title', 'Designation')  !!} <span class="text-danger">*</span>
            {!! Form::text('designation_title', null, ['class' => 'form-control', 'placeholder' => 'Enter Designation Title', 'id' => 'designation_title']) !!}

            @error('designation_title')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Status') !!} <span class="text-danger">*</span>
            {!! Form::select('status', ['1' => 'Active', '2' => 'Disable'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
