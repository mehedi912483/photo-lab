<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Name')  !!} <span class="text-danger">*</span>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Supplier Name', 'id' => 'name']) !!}

            @error('name')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('phone', 'Phone')  !!} <span class="text-danger">*</span>
            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number', 'id' => 'phone']) !!}

            @error('phone')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('email', 'E-mail')  !!} <span class="text-danger">*</span>
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter E-mail', 'id' => 'email']) !!}

            @error('email')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('company_name', 'Company')  !!}
            {!! Form::text('company_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Compay Name', 'id' => 'company_name']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('contact_person', 'Contact Person')  !!}
            {!! Form::text('contact_person', null, ['class' => 'form-control', 'placeholder' => 'Enter Contact Person', 'id' => 'contact_person']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('address', 'Address') !!}
            {!! Form::textarea('address', null, ['class' => 'form-control', 'id' => 'address']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Status') !!} <span class="text-danger">*</span>
            {!! Form::select('status', ['1' => 'Active', '2' => 'Disable'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
