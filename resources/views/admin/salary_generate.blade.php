@extends('layouts.admin')

@section('content')
<div class="main-panel">
      <div class="content">
        <div class="page-inner">
          <div class="page-header">
            <ul class="breadcrumbs">
              <li class="nav-home">
                <a href="#">
                  <i class="flaticon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Admin</a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Salary Generate</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">
                    Salary Generate
                  </div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                          <label for="comment">Working Days</label>
                          <input type="number" name="" placeholder="working days" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                          <label for="comment">From Date</label>
                          <input type="date" name="" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="comment">To Date</label>
                        <input type="date" name="" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label for="comment">&nbsp;</label>
                        <button class="btn btn-success">Generate Salary</button>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Stuff Name</th>
                          <th>Presen Day</th>
                          <th>Leave Day</th>
                          <th>Salary Amount</th>
                          <th>Total Salary</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>Md. Kamrul</td>
                          <td>28</td>
                          <td>3</td>
                          <td>50000,oo</td>
                          <td>50000,oo</td>
                        </tr>
                        <tr>
                          <th scope="row">1</th>
                          <td>Md. Kamal</td>
                          <td>28</td>
                          <td>5</td>
                          <td>50000,oo</td>
                          <td>50000,oo</td>
                        </tr>
                        <tr>
                          <th scope="row">1</th>
                          <td>Md. Jamal</td>
                          <td>28</td>
                          <td>2</td>
                          <td>50000,oo</td>
                          <td>50000,oo</td>
                        </tr>
                        <tr>
                          <th scope="row">1</th>
                          <td>Md. Rahim</td>
                          <td>28</td>
                          <td>1</td>
                          <td>50000,oo</td>
                          <td>50000,oo</td>
                        </tr>
                        <tr>
                          <th scope="row">1</th>
                          <td>Md. Karim</td>
                          <td>28</td>
                          <td>3</td>
                          <td>50000,oo</td>
                          <td>50000,oo</td>
                        </tr>
                        <tr>
                          <td colspan="6">
                            <button class="btn btn-success">Save</button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Salary</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="comment">Stuff Name</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option value="1">Select Stuff</option>
                    <option value="1">Stuff-1</option>
                    <option value="1">Stuff-2</option>
                    <option value="1">Stuff-3</option>
                    <option value="1">Stuff-4</option>
                    <option value="1">Stuff-5</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="comment">Present Day</label>
                  <input type="text" name="" class="form-control">
              </div>
              <div class="form-group">
                  <label for="comment">Salary</label>
                  <input type="number" name="" class="form-control">
              </div>
              <div class="form-group">
                  <label for="comment">Total Salary</label>
                  <input type="number" name="" class="form-control">
              </div>
              <div class="form-group">
                  <label for="comment">Status</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option value="1">Select Status</option>
                    <option value="1">Paid</option>
                    <option value="1">Pening</option>
                  </select>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection


@push('js')
<!-- Chart JS -->
<script src="{{ asset('assets/js/plugin/chart.js/chart.min.js') }}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Chart Circle -->
<script src="{{ asset('assets/js/plugin/chart-circle/circles.min.js') }}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('assets/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

<!-- Google Maps Plugin -->
<script src="{{ asset('assets/js/plugin/gmaps/gmaps.js') }}"></script>
@endpush