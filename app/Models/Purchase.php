<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['date', 'voucher_no', 'item_type', 'supplier_id', 'discount', 'grant_total', 'description'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->select('id', 'name');

    }

    public function supplierInfo()
    {
        return $this->belongsTo(Supplier::class)->select('name', 'phone', 'email', 'company_name', 'contact_person', 'address');

    }
    public function categories()
    {
        return $this->belongsTo(Category::class)->select('id', 'category_name');

    }

    public function items()
    {
        return $this->belongsTo(Item::class)->select('id', 'item_name');

    }

    public function purchaseDetails()
    {
        return $this->hasMany(PurchaseDetails::class)->select('item_id', 'quantity', 'unit_price');
    }
}
