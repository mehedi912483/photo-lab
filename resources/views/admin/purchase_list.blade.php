@extends('layouts.admin')

@section('content')
<div class="main-panel">
      <div class="content">
        <div class="page-inner">
          <div class="page-header">
            <ul class="breadcrumbs">
              <li class="nav-home">
                <a href="#">
                  <i class="flaticon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Admin</a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Purchase</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">
                    Purchase
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Supplier Name</th>
                        <th>Voucher No</th>
                        <th>Quantity</th>
                        <th>Paid Amount</th>
                        <th>Due Amount</th>
                        <th>Discount</th>
                        <th>Total Amount</th>
                        <th>Remarks</th>
                        <th>Status</th>
                        <th width="140">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>8-12-2020</td>
                        <td>Md Rahim</td>
                        <td>vc-5647</td>
                        <td>20.00</td>
                        <td>100.00</td>
                        <td>500.00</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td>purchase remarks</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>8-12-2020</td>
                        <td>Md Rahim</td>
                        <td>vc-5647</td>
                        <td>20.00</td>
                        <td>100.00</td>
                        <td>500.00</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td>purchase remarks</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>8-12-2020</td>
                        <td>Md Rahim</td>
                        <td>vc-5647</td>
                        <td>20.00</td>
                        <td>100.00</td>
                        <td>500.00</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td>purchase remarks</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>8-12-2020</td>
                        <td>Md Rahim</td>
                        <td>vc-5647</td>
                        <td>20.00</td>
                        <td>100.00</td>
                        <td>500.00</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td>purchase remarks</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>8-12-2020</td>
                        <td>Md Rahim</td>
                        <td>vc-5647</td>
                        <td>20.00</td>
                        <td>100.00</td>
                        <td>500.00</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td>purchase remarks</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>8-12-2020</td>
                        <td>Md Rahim</td>
                        <td>vc-5647</td>
                        <td>20.00</td>
                        <td>100.00</td>
                        <td>500.00</td>
                        <td>100.00</td>
                        <td>300.00</td>
                        <td>purchase remarks</td>
                        <td>Active</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection


@push('js')
<!-- Chart JS -->
<script src="{{ asset('assets/js/plugin/chart.js/chart.min.js') }}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Chart Circle -->
<script src="{{ asset('assets/js/plugin/chart-circle/circles.min.js') }}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('assets/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

<!-- Google Maps Plugin -->
<script src="{{ asset('assets/js/plugin/gmaps/gmaps.js') }}"></script>
@endpush