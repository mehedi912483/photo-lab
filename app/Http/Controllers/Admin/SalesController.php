<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\MoneyReceipt;
use App\Models\Sale;
use App\Models\SalesDetails;
use App\Models\Customer;
use App\Models\Item;
use App\Models\Category;
use App\Models\Stock;
use Auth;
use Illuminate\Http\Request;
use DB;
use Exception;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['salesList'] = Sale::with('customer')->paginate(20);
        return view('admin.sales.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customers'] = Customer::pluck('name', 'id');
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');

        return view('admin/sales/create', $data);
    }

    public function salesItem(Sale $sale)
    {
        //dd($sale);
        //DB::enableQueryLog();
        $data['salesList'] = SalesDetails::with('item')->where('sale_id', $sale->id)->get();
        //dd(DB::getQueryLog());
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['salesInfo'] = $sale;
        //dd($sale);

        return view('admin/sales/sales_item', $data);
    }

    public function itemInfo($itemId)
    {
        $itemInfo = Item::where('id', $itemId)->first();
        return response()->json($itemInfo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'customer_id' => ['required', 'integer', 'max:10'],
                'item_type' => ['required', 'integer'],
            ]
        );

        $sales = new Sale();
        $sales->fill($request->all());
        $sales->created_by = Auth::user()->id;
        $sales->save();

        toast('Please add your products!', 'success');

        return redirect()->route('sales.item', $sales->id);

        /*} catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }*/
    }

    public function storeItem(Sale $sale, Request $request)
    {

        //dd($sale);
        $request->validate(
            [
                'item_id' => ['required', 'integer'],
                'quantity' => ['required', 'integer'],
                'unit_price' => ['required', 'integer'],
            ]
        );

        try {
            DB::beginTransaction();
            $availableStock = Stock::where('item_id', $request->item_id)->first();
            if($availableStock->quantity >= $request->quantity) {
                $details = new SalesDetails();
                $details->fill($request->all());
                $details->sale_id = $sale->id;
                $details->total = $request->quantity * $request->unit_price;
                $details->created_by = Auth::user()->id;
                $details->save();

                $stock = Stock::deductStockReject($request->item_id, $request->quantity);
                $total = SalesDetails::where('sale_id', $sale->id)->sum('total');
                //DB::enableQueryLog();
                $sale->grand_total = $total - (($total * $sale->discount) / 100);
                $sale->save();
                DB::commit();
                toast('Product successfully added', 'success');
                return redirect()->route('sales.item', $sale->id);
            } else {
                toast('You have not sufficient quantity', 'warning');
                return redirect()->route('sales.item', $sale->id);
            }

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    public function itemUpdate(Request $request, SalesDetails $salesDetails)
    {

        $request->validate(
            [
                'item_id' => ['required','numeric', 'max:300'],
                'quantity' => ['required', 'numeric'],
                'unit_price' => ['required', 'numeric', 'digits_between:1,11'],
            ]
        );

        //dd($salesDetails);

        try {
            DB::beginTransaction();
            $availableStock = Stock::where('item_id', $request->item_id)->first();
            if($availableStock->quantity >= $request->quantity) {

                $oldQuantity = $salesDetails->quantity;
                $salesDetails->fill($request->all());
                $salesDetails->sale_id = $salesDetails->sale_id;
                $salesDetails->total = $request->quantity * $request->unit_price;
                $salesDetails->created_by = Auth::user()->id;
                $salesDetails->save();

                Stock::updateStockReject($request->item_id, $request->quantity, $oldQuantity);
                $sales = Sale::where('id', $salesDetails->sale_id)->first();
                $total = SalesDetails::where('sale_id', $salesDetails->sale_id)->sum('total');
                $sales->grand_total = $total - (($total * $sales->discount) / 100);
                $sales->save();

                DB::commit();

                toast('Sales Item successfully Update', 'success');
                return redirect()->route('sales.item', $salesDetails->sale_id);

            } else {

                toast('You have no available quantity!', 'warning');
                return redirect()->route('sales.item', $salesDetails->sale_id);
            }

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['salesDetails'] = SalesDetails::with('item')->where('sale_id', $id)->get();
        $data['invoiceTotal']    = SalesDetails::where('sale_id', $id)->sum('total');
        $data['totalReceive']    = MoneyReceipt::where('sale_id', $id)->sum('amount');
        //DB::enableQueryLog();
        $data['sales']        = Sale::where('id', $id)->first();
        $data['customerInfo']    = Customer::where('id', $data['sales']->customer_id)->first();
        //dd(DB::getQueryLog());

        return view('admin/sales/invoice_details', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //dd($sale);
        $data['customers'] = Customer::pluck('name', 'id');
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['sales'] = $sale;
        return view('admin/sales/edit', $data);
    }

    public function itemEdit(Sale $sale, SalesDetails $salesDetails)
    {
        $data['salesList'] = SalesDetails::with('item')->where('sale_id', $sale->id)->get();
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['salesInfo'] = $sale;
        $item = Item::where('id', $salesDetails->item_id)->first();
        $salesDetails->category_id = $item->category_id;
        $data['salesDetails'] = $salesDetails;

        return view('admin/sales/sales_item', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {

        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'customer_id' => ['required', 'integer', 'max:10'],
                'item_type' => ['required', 'integer'],
            ]
        );
        $total = SalesDetails::where('sale_id', $sale->id)->sum('total');
        //dd($sale);

        $sale->grand_total = $total - (($total * $request->discount)/100);
        $sale->save();

        $sale->fill($request->all());
        $sale->created_by = Auth::user()->id;
        $sale->save();


        toast('Sales Update Successfully, Please edit your products!', 'success');

        return redirect()->route('sales.item', $sale->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {

        foreach ($sale->salesDetails as $item){

            $stock = Stock::addStock($item->item_id, $item->quantity);
            $sale->salesDetails()->delete();
        }
        $sale->delete();

        toast('Sales has been deleted successfully!', 'success');

        return redirect()->route('sales.index');
    }

    public function detailsDestroy(Sale $sale, SalesDetails $salesDetails)
    {

        $oldQuantity = $salesDetails->quantity;
        $stock = Stock::addStock($salesDetails->item_id, $salesDetails->quantity);
        $salesDetails->delete();
        toast('Item Deduct successfully!', 'success');

        return redirect()->route('sales.item', $sale->id);
    }

    public function itemsByCategory($catId)
    {
        $items = Item::where('category_id', $catId)->get();

        return response()->json($items);
    }
}
