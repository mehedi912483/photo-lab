@extends('layouts.auth')

@section('content')
<form method="post" action="{{ route('password.update') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
<div class="wrapper wrapper-login">
    <div class="container container-login animated fadeIn">
        <h3 class="text-center">Reset Password</h3>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-form">
            <div class="form-group form-floating-label">
                <input id="email" name="email" type="email" class="form-control input-border-bottom" required value="{{ request('email') }}">
                <label for="email" class="placeholder">Email</label>
            </div>
            <div class="form-group form-floating-label">
                <input id="password" name="password" type="password" class="form-control input-border-bottom" required>
                <label for="password" class="placeholder">Password</label>
                <div class="show-password">
                    <i class="flaticon-interface"></i>
                </div>
            </div>
            <div class="form-group form-floating-label">
                <input id="password_confirmation" name="password_confirmation" type="password" class="form-control input-border-bottom" required>
                <label for="password_confirmation" class="placeholder">Confirm Password</label>
                <div class="show-password">
                    <i class="flaticon-interface"></i>
                </div>
            </div>
            <div class="form-action mb-3">
                <button class="btn btn-primary btn-rounded btn-login">Change Password</button>
            </div>
        
        </div>
    </div>


</div>
</form>
@endsection
    
