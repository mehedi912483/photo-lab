<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesDetails extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['sales_id','item_id', 'quantity', 'unit_price', 'total'];

    public function item()
    {
        return $this->belongsTo(Item::class)->select('id', 'item_name');
    }
}
