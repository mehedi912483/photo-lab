<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);

Auth::routes();
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function(){

    Route::resource('category', App\Http\Controllers\Admin\CategoryController::class);
    Route::resource('item', App\Http\Controllers\Admin\ItemController::class);
    Route::resource('supplier', App\Http\Controllers\Admin\SupplierController::class);
    Route::resource('customer', App\Http\Controllers\Admin\CustomerController::class);
    Route::resource('designation', App\Http\Controllers\Admin\DesignationController::class);
    Route::resource('opening-stock', App\Http\Controllers\Admin\OpeningStockController::class);
    Route::resource('stock-reject', App\Http\Controllers\Admin\StockRejectController::class);
    Route::resource('purchase', App\Http\Controllers\Admin\PurchaseController::class);
    Route::post('purchase/{purchase}/item/store', [App\Http\Controllers\Admin\PurchaseController::class, 'storeItem'])->name('purchase.item.store');
    Route::put('item-update/{purhaseDetails}', [App\Http\Controllers\Admin\PurchaseController::class, 'itemUpdate'])->name('item-update');
    Route::get('purchase/{purchase}/items', [App\Http\Controllers\Admin\PurchaseController::class, 'purchaseItem'])->name('purchase.item');
    Route::get('purchase/{purchase}/item/{purchaseDetails}/edit', [App\Http\Controllers\Admin\PurchaseController::class, 'itemEdit'])->name('purchase.details.edit');
    Route::get('purchase/{purchase}/item/{purchaseDetails}/delete', [App\Http\Controllers\Admin\PurchaseController::class, 'detailsDestroy'])->name('purchase.details.destroy');
    Route::get('items-by-category/{catId}', [App\Http\Controllers\Admin\OpeningStockController::class, 'itemsByCategory'])->name('items-by-category');
    Route::get('item-info/{itemId}', [App\Http\Controllers\Admin\PurchaseController::class, 'itemInfo'])->name('item-info');
    Route::resource('bill-payment', App\Http\Controllers\Admin\BillPaymentController::class);
    Route::get('invoice_due/{purchase_id}', [App\Http\Controllers\Admin\BillPaymentController::class, 'invoiceDue'])->name('invoice_due');

    Route::resource('sales', App\Http\Controllers\Admin\SalesController::class);
    Route::post('sales/{sale}/item/store', [App\Http\Controllers\Admin\SalesController::class, 'storeItem'])->name('sales.item.store');
    Route::put('sale-item-update/{salesDetails}', [App\Http\Controllers\Admin\SalesController::class, 'itemUpdate'])->name('sale-item-update');
    Route::get('sales/{sale}/items', [App\Http\Controllers\Admin\SalesController::class, 'salesItem'])->name('sales.item');
    Route::get('sales/{sale}/item/{salesDetails}/edit', [App\Http\Controllers\Admin\SalesController::class, 'itemEdit'])->name('sales.details.edit');
    Route::get('sales/{sale}/item/{salesDetails}/delete', [App\Http\Controllers\Admin\SalesController::class, 'detailsDestroy'])->name('sales.details.destroy');

    Route::resource('money-receipt', App\Http\Controllers\Admin\MoneyReceiptController::class);
    Route::get('sale_invoice_due/{sale_id}', [App\Http\Controllers\Admin\MoneyReceiptController::class, 'invoiceDue'])->name('invoice_due');

});

/*Route::get('admin/bill-payment', function () {
    return view('admin/bill_payment');
});*/

/*Route::get('admin/sales-list', function () {
    return view('admin/sales_list');
});
Route::get('admin/sales', function () {
    return view('admin/sales');
});*/
Route::get('admin/sales-expense', function () {
    return view('admin/sales_expense');
});
/*Route::get('admin/money-receipt', function () {
    return view('admin/money_receipt');
});*/
Route::get('admin/gate-pass', function () {
    return view('admin/gate_pass');
});
Route::get('admin/servicing-request', function () {
    return view('admin/servicing_request');
});
Route::get('admin/engineer-assign', function () {
    return view('admin/engineer_assign');
});
Route::get('admin/get-challan', function () {
    return view('admin/get_challan');
});
Route::get('admin/stuff', function () {
    return view('admin/stuff');
});
Route::get('admin/attendance', function () {
    return view('admin/attendance');
});
Route::get('admin/leave', function () {
    return view('admin/leave');
});
Route::get('admin/salary', function () {
    return view('admin/salary');
});
Route::get('admin/account-head', function () {
    return view('admin/account_head');
});
Route::get('admin/expense', function () {
    return view('admin/expense');
});
Route::get('admin/stuff-payment', function () {
    return view('admin/stuff_payment');
});
Route::get('admin/s', function () {
    return view('admin/salary');
});
Route::get('admin/salary-generate', function () {
    return view('admin/salary_generate');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function(){
	Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index']);
	//Route::get('/category', [App\Http\Controllers\Admin\DashboardController::class, 'index']);

	Route::get('/user', function () {
		return Auth::user();
	});
});
