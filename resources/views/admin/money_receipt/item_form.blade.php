<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            {!! Form::label('category_id', 'Category') !!}
            {!! Form::select('category_id', $categories, null, ['id' => 'category_id','name' => 'category_id', 'placeholder'=>'Select Category','class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('item_id', 'Item') !!}
            {!! Form::select('item_id', $items, null, ['id' => 'item_id', 'placeholder'=>'Select Item','class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('quantity', 'Quantity') !!}
            {!! Form::number('quantity', null, ['id' => 'quantity', 'placeholder'=>'Quantity','class' => 'form-control']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('unit_price', 'Quantity') !!}
            {!! Form::number('unit_price', null, ['id' => 'unit_price', 'placeholder'=>'Unit Price','class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-7">
        <table  class="table table-bordered">
            <thead>
            <th>Category</th>
            <th>Item</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total</th>
            <th>Action</th>
            </thead>
            <tbody class="item-table">

            </tbody>
        </table>
    </div>
    </div>
</div>
@push('js')
    <script>
        $('#category_id').on('change', function (){
            var category_id = $(this).val();
            $.ajax({
                url:"{{url('admin/items-by-category')}}/"+category_id,
                type:"GET",
                dataType:'json',
                success:function(data){
                    $('#item_id').html('<option value="">Select Item</option>')
                    $.each( data, function( key, value) {
                        $('#item_id').append('<option value="'+value.id+'">'+value.item_name+'</option>')
                    });
                }
            });
         });

        $('#item_id').on('change', function (){
            var item_id = $(this).val();
            $.ajax({
                url:"{{url('admin/item-info')}}/"+item_id,
                type:"GET",
                dataType:'json',
                success:function(data){
                   console.log(data);
                }
            });
        });
        $("#quantity,#unit_price").on('keyup',function (){
            console.log("hre now");
            var quantiy = $('#quantity').val();
            var price = $('#unit_price').val();
            $('#total').val(quantiy * price);
        });

        $('.add-item').on('click', function() {
            var itemId       = $('#item_id').val();
            var unitPrice    = $('#unit_price').val();
            var purQty       = $('#quantity').val();
            var proName      = $('#item_id option:selected').html();
            var totalPrice   = +unitPrice * +purQty;
            if((+itemId && +purQty) != ''){
                var html = '<tr>';
                html    += '<td class="serial"></td><td>'+proName+'</td><td class="text-right">'+purQty+'</td><td class="text-right">'+unitPrice+'</td><td class="text-right">'+totalPrice+'</td><td align="center">';
                html    += '<input type="hidden" name="proId[]" value="'+itemId+'" />';
                html    += '<input type="hidden" name="purQty[]" value="'+purQty+'" />';
                html    += '<input type="hidden" name="unitPrice[]" value="'+unitPrice+'" />';
                html    += '<input type="hidden" name="total[]" value="'+totalPrice+'" />';
                html    += '<a class="item-edit" href="#"><i class="fas fa-pen-square"></i></a>&nbsp;||&nbsp;<a class="item-delete text-danger" href="#"><i class="fas fa-trash-alt"></i></a></td></tr>';

                $('.item-table').append(html);
                $('#item_id').val('');
                $('#quantity').val('');
                $('#unit_price').val('');
                $('#total').val('');
                serialMaintain();
            } else {
                alert("Please Select Product and Quantity");
            }
        });
        $('.item-table').on('click', '.item-delete', function(e) {
            var element = $(this).parents('tr');
            element.remove();
            e.preventDefault();
            serialMaintain();
        });
        $('.item-table').on('click', '.item-edit', function(e) {
            var element        = $(this).parents('tr');
            var proId          = element.find('input[name="proId[]"]').val();
            var unitPrice      = element.find('input[name="unitPrice[]"]').val();
            var purQty         = element.find('input[name="purQty[]"]').val();
            var totalPrice     = element.find('input[name="total[]"]').val();

            $('#item_id').val(proId);
            $('#quantity').val(purQty);
            $('#unit_price').val(unitPrice);
            $('.total').val(totalPrice);
            element.remove();
            e.preventDefault();
            serialMaintain();
        });
        $('#discount').on('keyup', function (){
            var discount = $('#discount').val();
            var subtotal = $('#subTotal').val();
            var discountAmt = (discount * subtotal)/100;
            $('#grantTotal').val(subtotal - discountAmt);
        });
        function serialMaintain(){
            var i = 1;
            var subtotal = 0;
            $('.serial').each(function(key, element) {
                $(element).html(i);
                var total   = $(element).parents('tr').find('input[name="total[]"]').val();
                subtotal    += +total;
                i++;
            });

            $('#subTotal').val(subtotal);
            $('#grantTotal').val(subtotal);
            $('#grantTotal').trigger('change');
        }
    </script>
@endpush


