<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Auth;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['supplires'] = Supplier::paginate(20);
        return view('admin.supplier.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/supplier/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'name' => ['required', 'string', 'max:100'],
                'phone' => ['required', 'max:20'],
                'email' => ['required', 'max:150', 'email:rfc,dns', 'unique:suppliers,email'],
            ]
        );
        $supplier = new Supplier();

        $supplier->name           = $request->name;
        $supplier->phone          = $request->phone;
        $supplier->email          = $request->email;
        $supplier->company_name   = $request->company_name;
        $supplier->contact_person = $request->contact_person;
        $supplier->address        = $request->address;
        $supplier->status         = $request->status;
        $supplier->created_by     = Auth::user()->id;
        $supplier->save();

        toast('Supplier has been save successfully!', 'success');

        return redirect()->route('supplier.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        $data['supplier'] = $supplier;
        return view('admin/supplier/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        $request->validate(
            [
                'name' => ['required', 'string', 'max:100'],
                'phone' => ['required', 'max:20'],
                'email' => ['required', 'max:150', 'email:rfc,dns', 'unique:suppliers,email,'. $supplier->id . ',id'],
            ]
        );

        $supplier->name           = $request->name;
        $supplier->phone          = $request->phone;
        $supplier->email          = $request->email;
        $supplier->company_name   = $request->company_name;
        $supplier->contact_person = $request->contact_person;
        $supplier->address        = $request->address;
        $supplier->status         = $request->status;
        $supplier->created_by     = Auth::user()->id;
        $supplier->save();

        toast('Supplier has been updated successfully!', 'success');

        return redirect()->route('supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();

        toast('Supplier has been deleted successfully!', 'success');

        return redirect()->route('supplier.index');
    }
}
