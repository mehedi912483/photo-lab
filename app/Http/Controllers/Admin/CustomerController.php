<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Auth;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['customers'] = Customer::paginate(20);
        return view('admin.customer.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/customer/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'name' => ['required', 'string', 'max:100'],
                'phone' => ['required', 'max:20'],
                'email' => ['required', 'max:150', 'email:rfc,dns', 'unique:customers,email'],
            ]
        );
        $customer = new Customer();

        $customer->name           = $request->name;
        $customer->phone          = $request->phone;
        $customer->email          = $request->email;
        $customer->company_name   = $request->company_name;
        $customer->contact_person = $request->contact_person;
        $customer->address        = $request->address;
        $customer->status         = $request->status;
        $customer->created_by     = Auth::user()->id;
        $customer->save();

        toast('Customer has been save successfully!', 'success');

        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $data['customer'] = $customer;
        return view('admin/customer/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $request->validate(
            [
                'name' => ['required', 'string', 'max:100'],
                'phone' => ['required', 'max:20'],
                'email' => ['required', 'max:150', 'email:rfc,dns', 'unique:customers,email,'.$customer->id.',id'],
            ]
        );
        $customer->name           = $request->name;
        $customer->phone          = $request->phone;
        $customer->email          = $request->email;
        $customer->company_name   = $request->company_name;
        $customer->contact_person = $request->contact_person;
        $customer->address        = $request->address;
        $customer->status         = $request->status;
        $customer->created_by     = Auth::user()->id;
        $customer->save();

        toast('Customer has been updated successfully!', 'success');

        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        toast('Customer has been deleted successfully!', 'success');

        return redirect()->route('customer.index');
    }
}
