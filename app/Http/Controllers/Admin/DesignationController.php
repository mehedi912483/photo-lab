<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Designation;
use Auth;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data['designations'] = Designation::paginate(20);

        return view('admin/designation/list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin/designation/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'designation_title' => 'required|unique:designations,designation_title',
            ]
        );

        $designation = new Designation();

        $designation->designation_title = $request->designation_title;
        $designation->description   = $request->description;
        $designation->status        = $request->status;
        $designation->created_by    = Auth::user()->id;
        $designation->save();

        toast('Designation has been saved successfully!','success');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Designation $designation
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Designation $designation)
    {
        $data['designation'] = $designation;

        return view('admin/designation/edit_form', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Designation $designation)
    {
        $data['designation'] = $designation;

        return view('admin/designation/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Designation $designation)
    {
        $request->validate(
            [
                'designation_title' => ['required', 'string', 'max:100', 'unique:designations,designation_title,' . $designation->id . ',id'],
            ]
        );

        $designation->designation_title = $request->designation_title;
        $designation->description   = $request->description;
        $designation->status        = $request->status;
        $designation->created_by    = Auth::user()->id;
        $designation->save();

        toast('Designation has been updated successfully!','success');

        return redirect()->route('designation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Designation $designation
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Designation $designation)
    {
        $designation->delete();

        toast('Designation has been deleted successfully!','success');

        return redirect()->route('designation.index');
    }
}
