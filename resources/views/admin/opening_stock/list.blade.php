@extends('layouts.admin')

@section('content')
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <div class="page-header">
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#">
                                <i class="flaticon-home"></i>
                            </a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Admin</a>
                        </li>
                        <li class="separator">
                            <i class="flaticon-right-arrow"></i>
                        </li>
                        <li class="nav-item">
                            <a href="#">Opening Stock</a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Opening Stock
                                    <a href="{{ route('opening-stock.create') }}" class="btn btn-primary btn-sm pull-right">
                                        <i class="fas fa-plus-circle"></i> add
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category Name</th>
                                        <th>Item Name</th>
                                        <th>Quantity</th>
                                        <th>Description</th>
                                        <th width="140">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($openingStocks as $index => $v)
                                        <tr>
                                            <th scope="row">{{ $openingStocks->firstItem() + $index }}</th>
                                            <td>{{ $v->category->category_name }}</td>
                                            <td>{{ $v->item->item_name }}</td>
                                            <td>{{ $v->quantity }}</td>
                                            <td>{{ $v->description }}</td>
                                            <td>

                                                <form action="{{ route('opening-stock.destroy', $v->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <a href="{{ route('opening-stock.edit', $v->id) }}" class="btn btn-secondary btn-xs">
                                                        <i class="fas fa-pen-square"></i> Edit
                                                    </a>
                                                    <button class="btn btn-danger btn-xs delete-button">
                                                        <i class="fas fa-trash-alt"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $openingStocks->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        // mortgage approval
        $(document).on('click', '.delete-button', function (e) {
            e.preventDefault();
            Swal.fire({
                title: "Are you sure to delete",
                icon: "warning",
                confirmButtonText: 'Confirm',
                showCancelButton: true,
            }).then(function (result) {
                console.log(result)
                if (result.isConfirmed) $('.delete-button').parents('form').submit();
            });
        });
    </script>
@endpush
