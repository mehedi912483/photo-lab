<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MoneyReceipt extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['date', 'customer_id', 'sale_id', 'amount', 'description'];

    public function customer()
    {
        return $this->belongsTo(Customer::class)->select('id', 'name');
    }
}
