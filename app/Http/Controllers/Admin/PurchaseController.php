<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BillPayment;
use App\Models\Item;
use App\Models\Category;
use App\Models\Purchase;
use App\Models\PurchaseDetails;
use App\Models\Stock;
use App\Models\Supplier;
use Auth;
use Illuminate\Http\Request;
use DB;
use Exception;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['purchaseList'] = Purchase::with('supplier')->paginate(20);
        return view('admin.purchase.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['suppliers'] = Supplier::pluck('name', 'id');
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');

        return view('admin/purchase/create', $data);
    }

    public function purchaseItem(Purchase $purchase)
    {
        //dd($purchase);
        $data['purchaseList'] = purchaseDetails::with('item')->where('purchase_id', $purchase->id)->get();
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['purchaseInfo'] = $purchase;
        //dd($data['purchaseInfo']);

        return view('admin/purchase/purchase_item', $data);
    }

    public function itemInfo($itemId)
    {
        $itemInfo = Item::where('id', $itemId)->first();
        return response()->json($itemInfo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'supplier_id' => ['required', 'integer', 'max:10'],
                'item_type' => ['required', 'integer'],
            ]
        );

        //dd($request);

        //try {
        //DB::beginTransaction();

        $purchase = new Purchase();
        $purchase->fill($request->all());
        $purchase->created_by = Auth::user()->id;
        $purchase->save();

        //$stock = Stock::addStock($request->item_id, $request->quantity);

        //DB::commit();

        toast('Please add your products!', 'success');

        return redirect()->route('purchase.item', $purchase->id);

        /*} catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }*/
    }

    public function storeItem(Purchase $purchase, Request $request)
    {
        $request->validate(
            [
                'item_id' => ['required', 'integer'],
                'quantity' => ['required', 'integer'],
                'unit_price' => ['required', 'integer'],
            ]
        );

        //dd($request);

        try {
            DB::beginTransaction();

            $details = new PurchaseDetails();
            $details->fill($request->all());
            $details->purchase_id = $purchase->id;
            $details->total = $request->quantity * $request->unit_price;
            $details->created_by = Auth::user()->id;
            $details->save();

            $stock = Stock::addStock($request->item_id, $request->quantity);

            $total = PurchaseDetails::where('purchase_id', $purchase->id)->sum('total');

            //DB::enableQueryLog();
            $purchase->grant_total = $total - (($total * $purchase->discount)/100);
            $purchase->save();

            //dd(DB::getQueryLog());

            DB::commit();

            toast('Product successfully added', 'success');

            return redirect()->route('purchase.item', $purchase->id);

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    public function itemUpdate(Request $request, PurchaseDetails $purhaseDetails)
    {

        $request->validate(
            [
                'item_id' => ['required','numeric', 'max:300'],
                'quantity' => ['required', 'numeric'],
                'unit_price' => ['required', 'numeric', 'digits_between:1,11'],
            ]
        );

        try {
            DB::beginTransaction();

            $oldQuantity = $purhaseDetails->quantity;
            $purhaseDetails->fill($request->all());
            $purhaseDetails->purchase_id = $purhaseDetails->purchase_id;
            $purhaseDetails->total = $request->quantity * $request->unit_price;
            $purhaseDetails->created_by = Auth::user()->id;
            $purhaseDetails->save();

            $stock = Stock::updateStock($request->item_id, $request->quantity, $oldQuantity);

            $purchase = Purchase::where('id', $purhaseDetails->purchase_id)->first();

            //dd($purchase);

            $total = PurchaseDetails::where('purchase_id', $purhaseDetails->purchase_id)->sum('total');

            //DB::enableQueryLog();
            $purchase->grant_total = $total - (($total * $purchase->discount)/100);
            $purchase->save();

            //dd(DB::getQueryLog());

            DB::commit();

            toast('Purchase Item successfully Update', 'success');

            return redirect()->route('purchase.item', $purhaseDetails->purchase_id);

        } catch (Exception $exception) {
            DB::rollback();
            toast($exception->getMessage(), 'error');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['purchaseDetails'] = purchaseDetails::with('item')->where('purchase_id', $id)->get();
        $data['invoiceTotal']    = purchaseDetails::where('purchase_id', $id)->sum('total');
        $data['totalReceive']    = BillPayment::where('purchase_id', $id)->sum('amount');
        //DB::enableQueryLog();
        $data['purchase']        = Purchase::where('id', $id)->first();
        $data['supplierInfo']    = Supplier::where('id', $data['purchase']->supplier_id)->first();
        //dd(DB::getQueryLog());

        return view('admin/purchase/invoice_details', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //dd($purchase);
        $data['suppliers'] = Supplier::pluck('name', 'id');
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['purchase'] = $purchase;
        return view('admin/purchase/edit', $data);
    }

    public function itemEdit(Purchase $purchase, PurchaseDetails $purchaseDetails)
    {
        $data['purchaseList'] = purchaseDetails::with('item')->where('purchase_id', $purchase->id)->get();
        $data['categories'] = Category::pluck('category_name', 'id');
        $data['items'] = Item::pluck('item_name', 'id');
        $data['purchaseInfo'] = $purchase;
        $item = Item::where('id', $purchaseDetails->item_id)->first();
        $purchaseDetails->category_id = $item->category_id;
        $data['purhaseDetails'] = $purchaseDetails;

        return view('admin/purchase/purchase_item', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //dd($purchase);
        $request->validate(
            [
                'date' => ['required', 'string', 'max:50'],
                'supplier_id' => ['required', 'integer', 'max:10'],
                'item_type' => ['required', 'integer'],
            ]
        );
        $total = PurchaseDetails::where('purchase_id', $purchase->id)->sum('total');

        //DB::enableQueryLog();
        $purchase->grant_total = $total - (($total * $purchase->discount)/100);
        $purchase->save();

        $purchase->fill($request->all());
        $purchase->created_by = Auth::user()->id;
        $purchase->save();


        toast('Purchase Update Successfully, Please edit your products!', 'success');

        return redirect()->route('purchase.item', $purchase->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {

        foreach ($purchase->purchaseDetails as $item){

            $stock = Stock::deductStockReject($item->item_id, $item->quantity);
            $purchase->purchaseDetails()->delete();
        }
        $purchase->delete();

        toast('Purchase has been deleted successfully!', 'success');

        return redirect()->route('purchase.index');
    }

    public function detailsDestroy(Purchase $purchase, PurchaseDetails $purchaseDetails)
    {
        //dd($purchaseDetails);
        $stock = Stock::deductStockReject($purchaseDetails->item_id, $purchaseDetails->quantity);
        $purchaseDetails->delete();

        toast('Item Deduct successfully!', 'success');

        return redirect()->route('purchase.item', $purchase->id);
    }

    public function itemsByCategory($catId)
    {
        $items = Item::where('category_id', $catId)->get();

        return response()->json($items);
    }
}
