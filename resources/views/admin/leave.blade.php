@extends('layouts.admin')

@section('content')
<div class="main-panel">
      <div class="content">
        <div class="page-inner">
          <div class="page-header">
            <ul class="breadcrumbs">
              <li class="nav-home">
                <a href="#">
                  <i class="flaticon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Admin</a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Leave</a>
              </li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title">
                    Leave
                    <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#exampleModal">
                      <i class="fas fa-plus-circle"></i> add
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Remarks</th>
                        <th width="140">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>2020-12-12</td>
                        <td>Md. Hamidul</td>
                        <td>Remarks Here...</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>2020-12-12</td>
                        <td>Md. Hamidul</td>
                        <td>Remarks Here...</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>2020-12-12</td>
                        <td>Md. Hamidul</td>
                        <td>Remarks Here...</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>2020-12-12</td>
                        <td>Md. Hamidul</td>
                        <td>Remarks Here...</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1</th>
                        <td>2020-12-12</td>
                        <td>Md. Hamidul</td>
                        <td>Remarks Here...</td>
                        <td>
                          <a href="#" class="btn btn-secondary btn-xs">
                              <i class="fas fa-pen-square"></i>
                            Edit
                          </a>
                          <a href="#" class="btn btn-danger btn-xs">
                              <i class="fas fa-trash-alt"></i>
                            Delete
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Leave</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="comment">From Date</label>
                  <input type="date" name="" class="form-control">
              </div>
              <div class="form-group">
                  <label for="comment">To Date</label>
                  <input type="date" name="" class="form-control">
              </div>
              <div class="form-group">
                  <label for="comment">Stuff Name</label>
                  <select class="form-control">
                    <option>Select Stuff</option>
                    <option>Mr. Rahim</option>
                    <option>Mr. Karim</option>
                    <option>Mr. Jamal</option>
                    <option>Mr. Kamal</option>
                  </select>
              </div>
              <div class="form-group">
                <label for="comment">Remarks</label>
                <textarea class="form-control" id="comment" rows="2"></textarea>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection


@push('js')
<!-- Chart JS -->
<script src="{{ asset('assets/js/plugin/chart.js/chart.min.js') }}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Chart Circle -->
<script src="{{ asset('assets/js/plugin/chart-circle/circles.min.js') }}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('assets/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

<!-- Google Maps Plugin -->
<script src="{{ asset('assets/js/plugin/gmaps/gmaps.js') }}"></script>
@endpush